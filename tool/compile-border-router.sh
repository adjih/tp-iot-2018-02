#! /bin/sh -x
# C.A. 2018

CHANNEL=$1

if [ `hostname` != saclay ] ; then
    echo "Error: script should be run on an saclay.iot-lab.info" ; exit 1
fi

if [ -z "$CHANNEL" ] || [ "$CHANNEL" = "9" ] ; then
    echo "Error: syntax: $0 <channel>   (where channel is: 11 to 26)" ; exit 1
fi
    
cd ~/A8 || exit 1

# If not here: git clone RIOT version 2017.10
#    and put it in ~/A8/RIOT-2017-10
test -e RIOT-2017-10 || {
    git clone http://github.com/RIOT-OS/RIOT RIOT-2017-10 -b 2017.10-branch
} || exit 1

# If not here: compile RIOT border_router for iotlab-a8-m3 with correct channel
#    and put it in ~/A8/gnrc_border_router_ch<channel>.elf
test -e gnrc_border_router_ch${CHANNEL}.elf || (
    cd RIOT-2017-10/examples/gnrc_border_router \
       && make clean all DEFAULT_CHANNEL=${CHANNEL} BOARD=iotlab-a8-m3 \
       && mv bin/iotlab-a8-m3/gnrc_border_router.elf \
	      ~/A8/gnrc_border_router_ch${CHANNEL}.elf
) || exit 1
