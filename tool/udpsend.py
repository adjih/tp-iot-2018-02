#! /usr/bin/env python

import socket, argparse, time, sys

#-- Args

parser = argparse.ArgumentParser(
    description="send one or several times an UDP packet")
parser.add_argument("--port", type=int, default=61617,
                    help="destination port")
parser.add_argument("--count", type=int, default=1,
                    help="number of repetitions")
parser.add_argument("--delay", type=int, default=1,
                    help="delay (second) between repetitions")
parser.add_argument("dest", type=str, help="ipv6 address of the destination")
parser.add_argument("packet", help="content of the packet to send")
args = parser.parse_args()

# -- Prog
sd = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
for i in range(args.count):
    if sys.version_info < (3,0):
        raw_packet = bytes(args.packet)
    else:
        raw_packet = bytes(args.packet, "utf-8")
    sd.sendto(raw_packet, (args.dest, args.port))
    time.sleep(args.delay)
