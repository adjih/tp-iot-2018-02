#! /bin/sh -x
# C.A. 2018

CHANNEL=$1

if [ `uname -m` != armv7l ] ; then
    echo "Error: script should be run on an A8 node" ; exit 1
fi

if [ -z "$CHANNEL" ] || [ "$CHANNEL" = "9" ] ; then
    echo "Error: syntax: $0 <channel>    (where channel is: 11 to 26)" ; exit 1
fi
    
cd /home/root/A8 || exit 1

test -e gnrc_border_router_ch${CHANNEL}.elf || {
  echo "No firmware for border router in ~/A8 (gnrc_border_router-ch${CHANNEL}.elf)"
  exit 1
}

#

test -e RIOT-2017-10 || {
    git clone http://github.com/RIOT-OS/RIOT RIOT-2017-10 -b 2017.10-branch
} || exit 1

test -e RIOT-2017-10/dist/tools/uhcpd/bin/uhcpd  || (
  cd RIOT-2017-10/dist/tools/uhcpd && make
) || exit 1

test -e RIOT-2017-10/dist/tools/ethos/bin/ethos  || (
  cd RIOT-2017-10/dist/tools/ethos && make
) || exit 1

if [ "z$2" != "z--no-flash" ] ; then
    flash_a8_m3 gnrc_border_router_ch${CHANNEL}.elf || exit 1
else
    reset_a8_m3
fi

cd RIOT-2017-10/dist/tools/ethos  \
  && ./start_network.sh /dev/ttyA8_M3 tap0 ${INET6_PREFIX}::/64 115200
